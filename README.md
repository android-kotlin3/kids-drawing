# Kids Drawing 

The following application, it was sought to comply with a simple and intuitive interface to make drawings
maintain the state of the drawings and also share them with your friends

## Project Summary

In this application, we sought to make the concept of art or drawing for children, and in fact a niece of mine,
loved to draw on other drawings, well now with this application she does not leave the tablet with the images to entertain herself and draw.

But the important thing about this is to understand how to create a new view-type structure that allows you to
perform through a list of paths, save the drawings in memory, customize some actions, such as:

- [x] Change the brush color for drawing
- [x] Add a background image, to draw on top
- [x] Perform Undo action
- [x] Change brush size
- [x] Save and share drawing image made

In addition to the actions, it was sought to maintain the state of the Activity, even if the application executes the life state OnStop or OnPause

## Project Download

```
git clone https://gitlab.com/android-kotlin3/kids-drawing.git
```


## Solution to present:

Add three types of gifs to display:

- [x] use of interface.
- [x] usability.
- [x] simplicity.
- [x] Application permissions.
- [x] Import images from storage.
- [x] Export images to storage.
- [x] Share the images with your friends

***
## Demo Part 1
![](/markdown/demo01.gif "DEMO1 PROJECT")
***
## Demo Part 2
![](/markdown/demo02.gif "DEMO2 PROJECT")
***
## Demo Part 3
![](/markdown/demo03.gif "DEMO3 PROJECT")
